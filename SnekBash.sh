#!/usr/bin/env bash
COLS=$(tput cols);LINES=$(tput lines);X=$((COLS/2));Y=$((LINES/2));SNEK=( "$X,$Y" "$(((${X}-1))),$Y" "$(((${X}-2))),$Y" );DRTN='d';FRUIT=( q q );key='Q';
tput init; stty -echo
while : ;do
    read -t 1 -n 1 -s key && [[ " w a s d " =~ " ${key} " ]] && DRTN=$key
    [[ "$DRTN" == 'w' ]] && let "Y=Y-1" ; [[ "$DRTN" == 's' ]] && let "Y=Y+1" ; [[ "$DRTN" == 'a' ]] && let "X=X-1" ; [[ "$DRTN" == 'd' ]] && let "X=X+1" ;
    [[ " ${SNEK[*]} " =~ "$X,$Y" ]] && exit 1; [[ $Y -ge $LINES ]] || [[ $Y -lt 0 ]] && exit 1; [[ $X -ge $COLS ]] || [[ $X -lt 0 ]] && exit 1;
    [[ $(($(date +%s )%5)) == 0 ]] && [[ "${FRUIT[0]}" == "q" ]] && FRUIT=($((RANDOM%LINES)) $((RANDOM%COLS))) && tput cup "${FRUIT[0]}" "${FRUIT[1]}" && echo -n '@';
    [[ "$X,$Y" == "${FRUIT[1]},${FRUIT[0]}" ]] && oifs=$IFS && IFS=',' && SMO=( "${SNEK[-1]}" ) && SMT=( "${SNEK[-2]}" ) && IFS=$oifs && SNEK=( "$((SMO[0] - $((SMT[0]-SMO[0])))),$((SMO[1] - $((SMT[1]-SMO[1]))))" "${SNEK[@]}" ) && FRUIT=( q q )
    OCS=( $( echo "${SNEK[-1]}" | tr ',' ' ') );tput cup "${OCS[1]}" "${OCS[0]}";tput dch1;tput ich1;unset SNEK[-1];SNEK=( "$X,$Y" "${SNEK[@]}" );tput cup $Y $X; echo -n "#";
done
